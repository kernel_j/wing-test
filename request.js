"use strict"

const Axios = require('axios');

const getTrackingId = async function() {
    const url = 'https://helloacm.com/api/random/?n=15';

    try {
        const response = await Axios.get(url);
        const data = response.data;
        return (data);
    } catch(error) {
        console.log(error);
    }
}

module.exports = getTrackingId;
