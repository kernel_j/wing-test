# README #

This small program assembles parcels based on an order list. Items in the order have various weights and each parcel has a weight limit of 30.0kg.  

## INSTALL

### Dependencies
yarn 1.12.1
node 11.1.0

```js
yarn install

```

## USAGE

By running the index, a result.txt file will be create which contains the all the parcels of each order and the total operation cost.

```js
node index.js

```
