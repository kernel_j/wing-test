"use strict"

const FS = require('fs');
const getTrackingId = require('./request');

function findItemWeight(item_list, id) {
    if (Array.isArray(item_list) === false || typeof(id) !== 'string')
        return null; 
    for (let i = 0; i < item_list.length; i++) {
        if (item_list[i].id == id)
            return parseFloat(item_list[i].weight);
    }
    return null;
}

function preprocessData(items, item_list) {
    let items_processed = []
    let current_item = null
    let quantity = null

    items.forEach(function(item) {
        if (current_item === null) {
            const weight = findItemWeight(item_list, item.item_id);
            quantity = {
                item_id: item.item_id,
                quantity: item.quantity,
                weight: weight
            };
            current_item = item.item_id;
        } else if (current_item === item.item_id) {
            quantity.quantity += item.quantity;
        } else {
            items_processed.push(quantity);
            const weight = findItemWeight(item_list, item.item_id);
            quantity = {
                item_id: item.item_id,
                quantity: item.quantity,
                weight: weight
            };
            current_item = item.item_id;
        }
    })
    items_processed.push(quantity);

    return items_processed;
}

function sortWeight(item_list) {
    let new_item_list = JSON.parse(JSON.stringify(item_list));

    new_item_list.sort(function(a, b) {
        return b.weight - a.weight
    })
    return new_item_list;
}

function addItem(parcel, item) {
    let i = 0;
    while (i < parcel.items.length) {
        if (parcel.items[i].item_id === item.item_id) {
            parcel.items[i].quantity += 1;
            parcel.weight += item.weight;
            return ;
        }
        i++;
    }
    parcel.items.push({item_id: item.item_id, quantity: 1});
    parcel.weight += item.weight;
}

function orderHasStuffThatFits(items, remaining_weight) {
    let i = 0;
    while (i < items.length) {
        if (items[i].quantity > 0 && items[i].weight <= remaining_weight)
            return true;
        i++;
    }
    return false;
}

async function assembleParcel(items, order_id) {
    const tracking = await getTrackingId();
    const parcel = {
        order_id: order_id,
        items: [],
        weight: 0.0,
        tracking_id: tracking,
        palette_number: ''
    };
    
    let remaining_weight = 30.0 - parcel.weight;
    while (orderHasStuffThatFits(items, remaining_weight)) {
        for (let i = 0; i < items.length; i++) {
            if (items[i].quantity > 0 && items[i].weight <= remaining_weight) {
                addItem(parcel, items[i]);
                remaining_weight = 30.0 - parcel.weight;
                items[i].quantity -= 1;
            }
        }
    }
    return parcel;
}

function isOrderEmpty(order) {
    let i = 0;
    while (i < order.length) {
        if (order[i].quantity > 0)
            return false;
        i++;
    }
    return true;
}

async function divideOrder(item_list, items, order_id) {
    const pre_items = preprocessData(items, item_list);
    if (pre_items.length <= 0)
        return null;
    
    const sorted = sortWeight(pre_items);
    const total_parcels = [];
    
    while (!isOrderEmpty(sorted)) {
        const parcel = await assembleParcel(sorted, order_id);

        total_parcels.push(parcel);
    }
    return total_parcels;
}

function addPaletteNumber(parcels) {
    let counter = 0;
    let palette_number = 1;
    let i = 0;
    while (i < parcels.length) {
        let j = 0;
        while (j < parcels[i].length) {
            if (counter > 14) {
                palette_number++;
                counter = 0;
            }
            parcels[i][j].palette_number = palette_number;
            counter++;
            j++;
        }
        i++;
    }
}

function totalParcelCost(parcels) {
    let total = 0;
    let i = 0;
    while (i < parcels.length) {
        let j = 0;
        while (j < parcels[i].length) {
            switch (true) {
                case parcels[i][j].weight >= 0 && parcels[i][j].weight < 1.0:
                    total += 1;
                    break;
                case parcels[i][j].weight >= 1.0 && parcels[i][j].weight < 5.0:
                    total += 2;
                    break;
                case parcels[i][j].weight >= 5.0 && parcels[i][j].weight < 10.0:
                    total += 3;
                    break;
                case parcels[i][j].weight >= 10.0 && parcels[i][j].weight < 20.0:
                    total += 5;
                    break;
                case parcels[i][j].weight >= 20.0:
                    total += 10;
                    break;
            }
            j++;
        }
        i++;
    }
    return total;
}

async function main() {
    const items_contents = FS.readFileSync('./wing-test-data/items.json');
    const items = JSON.parse(items_contents);
    const orders_contents = FS.readFileSync('./wing-test-data/orders.json');
    const orders = JSON.parse(orders_contents);
    
    const map = orders.orders.map(async function(elem) {
        return divideOrder(items.items, elem.items, elem.id);
    })
    const result = await Promise.all(map);
    addPaletteNumber(result);
    const total = totalParcelCost(result);

    FS.writeFileSync('./result.txt', JSON.stringify(result, null, 2));
    FS.appendFileSync('./result.txt', '\n\ntotal operation cost: ' + total);
}

main();
